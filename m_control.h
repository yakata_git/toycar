/* 
 * File:   m_control.h
 * Author: yakata_fusion
 *
 * Created on December 2, 2020, 2:18 PM
 */
#include <stdint.h>
#include <stdbool.h>
#ifndef M_CONTROL_H
#define	M_CONTROL_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif
#define FLASH_ROW_ADDRESS     0x01C0
bool mainSW;
uint8_t subSW;
uint8_t cled;

uint16_t SPEED; //current velocity
uint16_t CUR_DIST;//current distance to wall
uint16_t CUR_DIST_TO_TARGET;//current distance to wall
uint16_t LINE1,LINE2;




typedef enum {
    FORWARD,
    STOP,  
    BACKWARD 
} CAR_STATE;

CAR_STATE carstate;

void m_control_init();
void m_control_switch_on(void);
void m_control_run(void);
void m_statecheck(CAR_STATE);
void m_set_speed();
void m_go_forward();
void m_go_backward();

uint16_t get_distance_to_wall(void);
uint16_t get_distnace_to_target(void);

void m_brake();

int16_t expo(int16_t);
void setVelocity(int16_t );
#endif	/* M_CONTROL_H */

